package ae.teletronics;

import ae.teletronics.ExternalAuth;
import org.reflections.Reflections;

import java.io.IOException;
import java.util.Optional;
import java.util.Set;

/**
 * Created by kristian on 3/28/16.
 */
public class Application {

    public static void main(String[] args) throws InstantiationException, IllegalAccessException, IOException, ClassNotFoundException {
        ae.teletronics.Application application = new ae.teletronics.Application();

        ae.teletronics.ExternalAuth externalAuth = application.getImplementation();
        externalAuth.setup();
        externalAuth.readInput();
    }

    ae.teletronics.ExternalAuth getImplementation() throws IllegalAccessException, InstantiationException, ClassNotFoundException {
        final Reflections reflections = new Reflections();
        final Set<Class<? extends ae.teletronics.ExternalAuth>> externalAuthImplementations = reflections.getSubTypesOf(ae.teletronics.ExternalAuth.class);
        final Optional<Class<? extends ae.teletronics.ExternalAuth>> implementationOptional = externalAuthImplementations.stream().findFirst();

        final Class<? extends ae.teletronics.ExternalAuth> implementationClass = implementationOptional.orElseThrow(() -> {
            return new ClassNotFoundException("There was no implementation of " + ExternalAuth.class.getName() + " supplied on classpath");
        });

        return implementationClass.newInstance();
    }



}
