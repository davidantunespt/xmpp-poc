package com.talkdesk.ejabberd;

import ae.teletronics.ExternalAuth;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;

/**
 * Created by davidantunes on 19/05/16.
 */
public class JwtAuth extends ExternalAuth {

    private static final String AUTH_API = "https://api.talkdesk.org/auth/access";
    private static final String CLIENT_ID = "56bb29fae5270e0003b6e786";
    private static final String CLIENT_VERSION = "xxx";
    private static final String CLIENT_SECRET = "8CgcvIScVVlNK7UWJYTqvurCMb4_0PrW0OlIFJ9FK6k=";

    public boolean authenticate(String user, String server, String password) {
        if(user.toLowerCase().contains("admin"))//workaround to keep "admin" as web admin console user
            return true;
        return authenticateJwtToken(password);
    }

    public boolean exists(String user, String server) {
        return true;
    }

    public boolean setPassword(String user, String s1, String s2) {
        return true;
    }

    public boolean register(String user, String s1, String s2) {
        return true;
    }

    public boolean remove(String user, String s1) {
        return true;
    }

    public boolean removeSafely(String user, String s1, String s2) {
        return true;
    }

    /**
     * Calls Auth API to validate the provided id_token
     *
     * @param id_token the id_token to be validated through Auth API
     * @return true if the id_token is valid; false otherwise
     */
    public static boolean authenticateJwtToken(String id_token){
        HttpURLConnection conn = null;

        try {
            URL url = new URL(AUTH_API);
            conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("POST");

            conn.setDoInput(true);
            conn.setDoOutput(true);

            String data = buildParams(
                    new Param("id_token",id_token),
                    new Param("client_id",CLIENT_ID),
                    new Param("client_version",CLIENT_VERSION),
                    new Param("client_secret",CLIENT_SECRET)
            );

            setBodyData(conn, data);

            return HttpURLConnection.HTTP_OK == conn.getResponseCode();

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            if(conn != null){
                conn.disconnect();
            }
        }
        return false;
    }

    /**
     * Places the data into the http post request body
     *
     * @param conn the HttpUrlConnection to be affeted
     * @param data the urlencoded string containing the data params required to perform the call
     * @throws IOException
     */
    private static void setBodyData(HttpURLConnection conn, String data) throws IOException {
        conn.setRequestProperty ("Content-Type", "application/x-www-form-urlencoded");
        OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
        wr.write(data);
        wr.close();
    }

    /**
     * Builds a string containing the Param elements provided as param in name/value pair format
     *
     * @param params the list of Param that will be returned as string
     * @return a string containing the params passed as parameter as urlencoded name/value pairs
     * @throws UnsupportedEncodingException
     */
    private static String buildParams(Param... params) throws UnsupportedEncodingException {
        String data = "";

        for ( Param par : params ) {
            if ( data.length() > 1 ) data += "&";
            data += URLEncoder.encode(par.name, "UTF-8") + "=" + URLEncoder.encode(par.value, "UTF-8");
        }

        return data;
    }

    private static class Param{
        private String name;
        private String value;

        public Param(String name, String value){
            this.name = name;
            this.value = value;
        }
    }
}

