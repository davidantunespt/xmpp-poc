var express = require('express');
var router = express.Router();

var Client = require('node-xmpp-client');

/* GET home page. */
router.get('/', function(req, res, next) {
    res.render('index', { title: 'XMPP Presences PoC' });
});

module.exports = router;

