var Client = {
    SERVICE: 'ws://localhost:5280/websocket',
    Presences: {ONLINE:"2",AWAY:"1",BUSY:"3",OFFLINE:"4"},

    domain: 'davids-macbook-pro.local',
    isConnected: false,
    connection: null,
    nick: null,
    roomName: 'operators@conference.davids-macbook-pro.local',

    log: function(msg, data){ //simple logging function
        console.log(msg + ';data=' + data);
    },

    handle_message: function(msg) {
        var to = msg.getAttribute('to');
        var from = msg.getAttribute('from');
        var nick = Strophe.getResourceFromJid(from);
        var timestamp = $(msg).find('delay').attr('stamp');
        var type = msg.getAttribute('type');

        var elems = msg.getElementsByTagName('body');

        if (elems.length > 0) {
            var body = elems[0];
            $(document).trigger('handle_message', {
                    from: nick,
                    text: Strophe.getText(body),
                    timestamp: timestamp
                }
            );
        }

        // we must return true to keep the handler alive.
        // returning false would remove it after it finishes.
        return true;
    },

    on_connect: function(status)
    {
        switch(status){
            case Strophe.Status.CONNECTING:
                Client.log('Strophe is connecting.');
                break;
            case Strophe.Status.CONNFAIL:
                Client.log('Strophe failed to connect.');
                break;
            case Strophe.Status.DISCONNECTING:
                Client.log('Strophe is disconnecting.');
                break;
            case Strophe.Status.DISCONNECTED:
                Client.log('Strophe is disconnected.');
                Client.isConnected = false;
                $(document).trigger('disconnected');
                break;
            case Strophe.Status.CONNECTED:
                Client.log('Strophe is connected');
                Client.nick = Client.retrieveNick(Client.connection.jid);
                Client.isConnected = true;

                Client.set_handlers();
                Client.request_roster();
                Client.send_ping();
                Client.send_online_presence();
                Client.join_chat(Client.nick);

                $(document).trigger('connected', {resource : Strophe.getResourceFromJid(Client.connection.jid)});
        }
    },

    error_callback: function(){
        Client.log("Error sending stanza!");
    },

    set_handlers: function(){
        Client.connection.addHandler(Client.handle_pong, null, "iq", null, "ping1");
        Client.connection.addHandler(Client.handle_version, null, "iq", null, "version1");
        Client.connection.addHandler(Client.handle_message, null, 'message', "groupchat", null,  null);
        Client.connection.addHandler(Client.on_roster_changed, "jabber:iq:roster", "iq", "set");
    },

    request_roster: function(){
        var iq = $iq({type: 'get'}).c('query', {xmlns: 'jabber:iq:roster'});
        Client.connection.sendIQ(iq, Client.on_roster, Client.error_callback);
    },

    send_online_presence: function(){
        Client.connection.send($pres());
    },

    send_ping: function (to) {
        to = to || Client.get_domain();

        var ping = $iq({
            to: to,
            type: "get",
            id: "ping1"
        }).c("ping", {xmlns: "urn:xmpp:ping"});

        Client.start_time = (new Date()).getTime();
        Client.connection.send(ping);
    },

    handle_pong: function (iq) {
        var elapsed = (new Date()).getTime() - Client.start_time;
        Client.log("Received pong from server in " + elapsed + "ms.");

        return true;
    },

    check_version: function(){
        Client.connection.send($iq({type: "get", id: "version1", to: "davids-macbook-pro.local"}) .c("query", {xmlns: "jabber:iq:version"}));
    },
    handle_version: function(iq){
        //todo..
        return true;
    },

    jid_to_id: function (jid) {
        return Strophe.getBareJidFromJid(jid)
            .replace(/@/g, "-")
            .replace(/\./g, "-");
    },

    on_roster: function (iq) {
        $(iq).find('item').each(function () {
            var jid = $(this).attr('jid');
            var name = $(this).attr('name') || jid;
            name = Client.retrieveNick(name);

            Client.manage_contact_element(jid,name);
        });

        // set up presence handler and send initial presence
        Client.connection.addHandler(Client.on_presence, null, "presence");
        Client.connection.send($pres());

        return true;
    },

    on_roster_changed: function (iq) {
        $(iq).find('item').each(function () {
            var sub = $(this).attr('subscription');
            var jid = $(this).attr('jid');
            var name = $(this).attr('name') || Strophe.getBareJidFromJid(jid);

            if (sub === 'remove') {
                // contact is being removed
                Client.remove_contact_element(jid);
            } else {
                Client.manage_contact_element(jid, name);
            }
        });

        return true;
    },

    remove_contact_element: function(jid){
        var jid_id = Client.jid_to_id(jid);
        $('#' + jid_id).remove();
    },

    manage_contact_element : function(jid, name){
        var jid_id = Client.jid_to_id(jid);
        var resource = Client.get_resource_from_jid(jid);

        var contact_html = "<li id='" + jid_id + "'>" +
            "<div class='" +
            ($('#' + jid_id).attr('class') || "roster-contact offline") +
            "'>" +
            "<div class='roster-name'>" +
            name +
            "</div><div class='roster-jid'>" +
            jid +
            "</div>" +
            "<div>" +
            "<div class='roster-resource'>" +
            resource +
            "</div>" +
            "<span class='roster-resource-status'>" +
            "</span>" +
            "</div>" +
            "</div></li>";

        if ($('#' + jid_id).length > 0) {
            $('#' + jid_id).replaceWith(contact_html);
        } else {
            Client.insert_contact($(contact_html));
        }
    },

    join_chat: function(nick){
        var to = Client.roomName + '/' + nick;
        var from = Client.connection.jid;
        Client.nick = nick;

        Client.connection.send(
            $pres({to: to, from: from}).c('x', {xmlns:'http://jabber.org/protocol/muc'}));
    },
    send_message: function(msg){
        var reply = $msg({to: Client.roomName, type: 'groupchat'})
            .c('body').t(msg);
        Client.connection.send(reply);
    },

    update_presence: function(presence, status){
        var p = $pres();//online
        status = status || "";

        switch(presence){
            case Client.Presences.BUSY:
                p = $pres().c('show').t('dnd').up().c('status').t(status);
                break;
            case Client.Presences.AWAY:
                p = $pres().c('show').t('away');
                break;
            case Client.Presences.OFFLINE:
                p = $pres({type:'unavailable'});
                break;
            default:
                //online
                Client.join_chat(Client.nick);
                break;
        }

        Client.connection.send(p);
        $(document).trigger('self_presence_update', {presence : presence, status : status});
    },

    connect: function(user, password, resource){
        var jid = user + '@' + Client.domain;
        if(!!resource){
            jid = jid + '/' + resource;
        }

        Client.resource = resource;
        Client.connection.connect(jid, password, Client.on_connect);
    },

    disconnect: function(){
        Client.connection.disconnect();
    },

    presence_value: function (elem) {
        if (elem.hasClass('online')) {
            return 2;
        } else if (elem.hasClass('away')) {
            return 1;
        }
        return 0;
    },

    get_resource_from_jid: function(jid){
        return Strophe.getResourceFromJid(jid) || "";
    },

    get_domain: function(){
        return Strophe.getDomainFromJid(Client.connection.jid) || "";
    },

    on_presence: function (presence) {
        //check if the presence is coming from a MUC
        if($(presence).find('x[xmlns="http://jabber.org/protocol/muc#user"]').length == 1){
            return true;
        }

        var from = $(presence).attr('from');
        var to = $(presence).attr('to');

        //we don't want to handle our own presence
        if(Strophe.getBareJidFromJid(from) === Strophe.getBareJidFromJid(to))
            return true;

        var ptype = $(presence).attr('type');
        var jid_id = Client.jid_to_id(from);
        var resource = Client.get_resource_from_jid(from);
        var status = $(presence).find('status').text().trim();

        if (ptype === 'subscribe') {
            //TODO: requesting presence subscription
            //not in the scope of this poc
        }
        else if (ptype !== 'error') {
            //if contact element is inexistent then create new
            if ($('#' + jid_id).length == 0) {
                Client.manage_contact_element(from, Client.retrieveNick(from));
            }

            var contact = $('#roster-area li#' + jid_id + ' .roster-contact')
                .removeClass("online")
                .removeClass("away")
                .removeClass("offline");

            //update resource
            Client.update_resource_element(ptype, contact, resource, status, presence);
            /*var resourceElem = $(contact).find('.roster-resource[data-resource-id="'+resource+'"]');

            if(resourceElem.length > 0){
                resourceElem.removeClass("online")
                    .removeClass("away")
                    .removeClass("offline");
            }
            else{
                $(contact).append('<div><span class="roster-resource" data-resource-id="'+resource+'">'
                    +resource+'</span>&nbsp;<span class="roster-resource-status small"></span></span></div>');
                resourceElem = $(contact).find('.roster-resource[data-resource-id="'+resource+'"]');
            }

            $(resourceElem).parent().find('.roster-resource-status').text(status);

            if (ptype === 'unavailable') {
                resourceElem.addClass("offline");
            } else {
                var show = $(presence).find("show").text();
                if (show === "" || show === "chat") {
                    resourceElem.addClass("online");
                } else {
                    resourceElem.addClass("away");
                }
            }*/

            Client.set_contact_presence_class(contact);
            /*if($(contact).find('.online').length > 0){
                $(contact).addClass('online');
            }
            else if($(contact).find('.away').length > 0){
                $(contact).addClass('away');
            }
            else $(contact).addClass('offline');

            var li = contact.parent();
            li.remove();
            Client.insert_contact(li);*/
        }
        else{
            Client.log("Error on_presence: " + presence);
        }

        return true;
    },

    set_contact_presence_class: function(contact){
        if($(contact).find('.online').length > 0){
            $(contact).addClass('online');
        }
        else if($(contact).find('.away').length > 0){
            $(contact).addClass('away');
        }
        else $(contact).addClass('offline');

        var li = contact.parent();
        li.remove();
        Client.insert_contact(li);
    },

    update_resource_element: function(ptype, contact, resource, status, presence){
        //update resource
        var resourceElem = $(contact).find('.roster-resource[data-resource-id="' + resource + '"]');

        if(resourceElem.length > 0){
            resourceElem.removeClass("online")
                .removeClass("away")
                .removeClass("offline");
        }
        else{
            $(contact).append('<div><span class="roster-resource" data-resource-id="' + resource + '">'
                + resource + '</span>&nbsp;<span class="roster-resource-status small"></span></span></div>');
            resourceElem = $(contact).find('.roster-resource[data-resource-id="' + resource + '"]');
        }

        $(resourceElem).parent().find('.roster-resource-status').text(status);

        if (ptype === 'unavailable') {
            resourceElem.addClass("offline");
        } else {
            var show = $(presence).find("show").text();
            if (show === "" || show === "chat") {
                resourceElem.addClass("online");
            } else {
                resourceElem.addClass("away");
            }
        }
    },

    insert_contact: function (elem) {
        var jid = elem.find('.roster-jid').text();
        var pres = Client.presence_value(elem.find('.roster-contact'));

        var contacts = $('#roster-area li');

        if (contacts.length > 0) {
            var inserted = false;
            contacts.each(function () {
                var cmp_pres = Client.presence_value(
                    $(this).find('.roster-contact'));
                var cmp_jid = $(this).find('.roster-jid').text();

                if (pres > cmp_pres) {
                    $(this).before(elem);
                    inserted = true;
                    return false;
                } else if (pres === cmp_pres) {
                    if (jid < cmp_jid) {
                        $(this).before(elem);
                        inserted = true;
                        return false;
                    }
                }
            });

            if (!inserted) {
                $('#roster-area ul').append(elem);
            }
        } else {
            $('#roster-area ul').append(elem);
        }
    },

    retrieveNick: function (username) {
        var at = username.indexOf('@');
        if(at != -1) 
            return username.substr(0, at);
        return username;
    }
}

$(document).bind('disconnected', function () {
    $('#disconnect').get(0).value = 'Connect';
    $('#roster-area ul').empty();
    $('#chat-area div').empty();
    $('#login-dialog').modal('show');
    $('.state-dropdown').addClass('hide');
    $('.status-dropdown').addClass('hide');
});

$(document).bind('connected', function (e, data) {
    $('#disconnect').get(0).value = 'Disconnect';
    $('.state-dropdown').removeClass('hide');
    $('#chat-input').keyup(function(e){
        if(e.keyCode == 13)
        {
            e.preventDefault();
            if(Client.isConnected) {
                var msg = $('#chat-input').val();
                if (msg.length > 0) {
                    Client.send_message(msg);
                    $('#chat-input').val('');
                }
            }
        }
    });

    if(data && data.resource){
        $('.resource-badge .resource-description').text(data.resource);
    }else {
        $('.resource-badge').addClass('hide');
    }

    setTitle(Client.nick, data.resource);
});

$(document).bind('handle_message', function (e, data) {
    var display_name = (Client.nick == data.from) ? 'me' : data.from;
    data.timestamp = data.timestamp ? moment(data.timestamp) : moment();
    var time = moment().format('LTS');
    $('#chat-area').append("<div>[" + time + "] <b>" + display_name + "</b>: " + data.text + "</div>");

    ca = document.getElementById("chat-area");
    ca.scrollTop = ca.scrollHeight;

    return true;
});

$(document).bind('self_presence_update', function(e ,data){
    if(!!data.status){
        $('span.current-status').text(data.status.trim() + ' ');
    }
    var pdd = $('.presence-dropdown');
    $(pdd).removeClass('btn-success');
    $(pdd).removeClass('btn-danger');
    $(pdd).removeClass('btn-warning');

    if(data.presence == Client.Presences.BUSY){
        $('span.current-presence').text("Busy ");
        $('.presence-dropdown').addClass('btn-warning');
    }
    if(data.presence == Client.Presences.AWAY){
        $('span.current-presence').text("Away ");
        $('.presence-dropdown').addClass('btn-warning');
    }
    else if(data.presence == Client.Presences.ONLINE){
        $('span.current-presence').text("Online ");
        $('.presence-dropdown').addClass('btn-success');
    }
    else if(data.presence == Client.Presences.OFFLINE){
        $('span.current-presence').text("Offline ");
        $('.presence-dropdown').addClass('btn-danger');
    }
})

$(document).ready(function () {
    connection = new Strophe.Connection(Client.SERVICE);
    connection.rawInput = rawInput;
    connection.rawOutput = rawOutput;

    Client.log = log;
    Client.connection = connection;

    prepareDropDowns();
    setClickEvents();

    $('#login-dialog').modal({show:true, keyboard:false,backdrop:'static'});
});

function getSelectedStatus(){
    return $('.current-status').text().trim();
}

function retrieveNick(username){
    var at = username.indexOf('@');
    if(at != -1) return username.substr(0, at);
    return username;
}

function setTitle(nick, resource){
    document.title = nick + ' (' + resource + ')';
}

function prepareDropDowns(){
    $('.state-dropdown ul li a').each(function(idx){
        $(this).bind('click', function(){
            var status = "";
            if($(this).attr('data-toggle-status')) {
                $('.status-dropdown').removeClass('hide');
                status = getSelectedStatus();
            }
            else {
                $('.status-dropdown').addClass('hide');
            }
            Client.update_presence($(this).attr('data-state'), status);
        });
    });

    $('.status-dropdown ul li a').each(function(idx){
        $(this).bind('click', function(){
            Client.update_presence(Client.Presences.BUSY, $(this).text());
        });
    });
}

function setClickEvents(){
    //disconnect button
    $('#disconnect').bind('click', function () {
        Client.disconnect();
    });

    //login button
    $('.login-button').on('click', function(){
        $('.form-group').removeClass('has-error');

        var resource = $('#resource :selected').attr('data-value');
        var username = $('#username').val();
        var password = $('#password').val();

        if(!username || !password) {
            $('.form-group.required').addClass('has-error');
            return false;
        }

        $.post( "/auth", { username: username, password: password })
            .done(function( data ) {
                var id_token = JSON.parse(data).id_token;
                var nick = Client.retrieveNick(username);
                Client.connect(nick, id_token, resource);
            }).fail(function(){
                $(document).trigger('disconnected');
            }
        );
    });

    //message text
    $('#chat-area').on('click', function(){
        $('#chat-area').parent().find('#chat-input').focus();
    });

    //login input fields
    $('#login-form input').keypress(function (e) {
        if (e.which == 13) {
            $('.login-button').click();
            return false;
        }
    });

    //ping button
    $('#ping').on('click', function(){
        Client.send_ping();
    });
}

function rawInput(data)
{
    Client.log('RECV', data);
}

function rawOutput(data)
{
    Client.log('SENT', data);
}

function log(msg, data) {
    var tr = document.createElement('tr');
    var th = document.createElement('th');
    th.setAttribute( "style", "text-align: left; vertical-align: top;" );
    var td;

    th.appendChild( document.createTextNode(msg) );
    tr.appendChild( th );

    if (data) {
        td = document.createElement('td');
        pre = document.createElement('code');
        pre.setAttribute("style", "white-space: pre-wrap;");
        td.appendChild(pre);
        pre.appendChild( document.createTextNode( vkbeautify.xml(data) ) );
        tr.appendChild(td);
    } else {
        th.setAttribute('colspan', '2');
    }

    $('#log').append(tr);

    d = document.getElementById("log");
    d.scrollTop = d.scrollHeight;
}